package com.melnykov;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;

public class MarkdownProcessorTest extends TestCase {

    private MarkdownProcessor markdownProcessor;

    public MarkdownProcessorTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(MarkdownProcessorTest.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        markdownProcessor = new MarkdownProcessor();
    }

    public void testEmptyInput() throws IOException {
        String input = FileUtils.readFile(getFileNameFromResources("empty.md")).trim();
        String expectedOutput = FileUtils.readFile(getFileNameFromResources("empty.html")).trim();

        String html = markdownProcessor.convertToHtml(input);
        assertEquals(expectedOutput, html);
    }

    public void testRegularInput() throws IOException {
        String input = FileUtils.readFile(getFileNameFromResources("regular.md")).trim();
        String expectedOutput = FileUtils.readFile(getFileNameFromResources("regular.html")).trim();

        String html = markdownProcessor.convertToHtml(input);
        assertEquals(expectedOutput, html);
    }

    public void testNullInput() {
        try {
            String html = markdownProcessor.convertToHtml(null);
            fail("Null input string must throw an exception.");
        } catch (NullPointerException e) {
            // OK
        }
    }

    public void testProcessingTime() throws IOException {
        long start = System.currentTimeMillis();
        String input = FileUtils.readFile(getFileNameFromResources("regular.md")).trim();

        for (int i = 0; i < 1000; i++) {
            markdownProcessor.convertToHtml(input);
        }
        long end = System.currentTimeMillis();
        System.out.printf("Markdown to html took %s ms\n", end - start);
    }

    private String getFileNameFromResources(String fileName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource(fileName).getFile();
    }
}