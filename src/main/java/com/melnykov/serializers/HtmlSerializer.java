package com.melnykov.serializers;

import com.melnykov.MarkdownSerializer;
import com.melnykov.Visitor;
import com.melnykov.nodes.*;

/**
 * This class serializes {@link RootNode} to the HTML string.
 */
public class HtmlSerializer implements MarkdownSerializer, Visitor {

    private final StringBuilder html;

    public HtmlSerializer() {
        this.html = new StringBuilder();
    }

    public String serialize(RootNode rootNode) {
        this.html.setLength(0);
        rootNode.accept(this);
        return html.toString();
    }

    public void visit(HeaderNode node) {
        println();
        html.append("<h").append(node.getLevel()).append(">")
                .append(node.getText())
                .append("</h").append(node.getLevel()).append(">");
    }

    public void visit(RootNode node) {
        html.append("<html>");
        println();
        html.append("<body>");
        visitChildren(node);
        println();
        html.append("</body>");
        println();
        html.append("</html>");
    }

    public void visit(EmphasizeNode node) {
        html.append("<em>").append(node.getText()).append("</em>");
    }

    public void visit(StrongNode node) {
        html.append("<strong>").append(node.getText()).append("</strong>");
    }

    public void visit(TextNode node) {
        html.append(node.getText());
    }

    public void visit(ParagraphNode node) {
        println();
        html.append("<p>");
        visitChildren(node);
        html.append("</p>");
    }

    public void visit(LinkNode node) {
        html.append("<a href=\"").append(node.getUrl()).append("\">").append(node.getText()).append("</a>");
    }

    private void visitChildren(SuperNode superNode) {
        for (Node child : superNode.getChildren()) {
            child.accept(this);
        }
    }

    private void println() {
        html.append(System.lineSeparator());
    }
}