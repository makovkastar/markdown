package com.melnykov.nodes;

import com.melnykov.Visitor;

/**
 * This class represents the markdown url link node.
 */
public class LinkNode extends TextNode {

    private final String url;

    public LinkNode(String text, String url) {
        super(text);
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "LinkNode{" +
                "url='" + url + '\'' +
                '}';
    }
}