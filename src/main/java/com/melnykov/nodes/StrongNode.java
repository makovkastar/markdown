package com.melnykov.nodes;


import com.melnykov.Visitor;

/**
 * This class represents the markdown strong text node.
 */
public class StrongNode extends TextNode {

    public StrongNode(String text) {
        super(text);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}