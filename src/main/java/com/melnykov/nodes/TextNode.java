package com.melnykov.nodes;

import com.melnykov.Visitor;

/**
 * This class represents a markdown node with text.
 */
public class TextNode implements Node {

    private final String text;

    public TextNode(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "TextNode{" +
                "text='" + text + '\'' +
                '}';
    }
}