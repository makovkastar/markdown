package com.melnykov.nodes;

import com.melnykov.Visitor;

import java.util.List;

/**
 * This class represents a root node in the markdown nodes hierarchy.
 */
public class RootNode extends SuperNode {

    public RootNode(List<Node> children) {
        super(children);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}