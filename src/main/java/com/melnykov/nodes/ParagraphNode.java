package com.melnykov.nodes;

import com.melnykov.Visitor;

import java.util.List;

/**
 * This class represents the markdown paragraph node.
 */
public class ParagraphNode extends SuperNode {

    public ParagraphNode(List<Node> children) {
        super(children);
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}