package com.melnykov.nodes;

import com.melnykov.Visitor;

public interface Node {
    void accept(Visitor visitor);
}