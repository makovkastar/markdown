package com.melnykov.nodes;

import com.melnykov.Visitor;

/**
 * This class represents the markdown header node.
 */
public class HeaderNode extends TextNode {

    private final int level;

    public HeaderNode(String text, int level) {
        super(text);
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return "HeaderNode{" +
                "level=" + level +
                '}';
    }
}