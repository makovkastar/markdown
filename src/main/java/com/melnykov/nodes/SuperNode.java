package com.melnykov.nodes;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is a base class for all markdown nodes which have child nodes.
 */
public abstract class SuperNode implements Node {

    private final List<Node> children = new ArrayList<Node>();

    public SuperNode() {
    }

    public SuperNode(Node child) {
        children.add(child);
    }

    public SuperNode(List<Node> children) {
        this.children.addAll(children);
    }

    public List<Node> getChildren() {
        return children;
    }
}