package com.melnykov.nodes;

import com.melnykov.Visitor;

/**
 * This class represents the markdown emphasize node.
 */
public class EmphasizeNode extends TextNode {

    public EmphasizeNode(String text) {
        super(text);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}