package com.melnykov;

import com.melnykov.nodes.RootNode;
import com.melnykov.parsers.MarkdownParser;
import com.melnykov.serializers.HtmlSerializer;

import java.io.IOException;

public class MarkdownProcessor {

    private final MarkdownParser markdownParser;
    private final MarkdownSerializer markdownHtmlSerializer;

    public MarkdownProcessor() {
        markdownParser = new MarkdownParser();
        markdownHtmlSerializer = new HtmlSerializer();
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            throw new IllegalArgumentException("You must specify an input file name as a first argument!");
        }

        String inputFileName = args[0];
        String outputFileName = null;
        if (args.length > 1) {
            outputFileName = args[1];
        }

        MarkdownProcessor markdownProcessor = new MarkdownProcessor();
        try {
            String markdownSource = FileUtils.readFile(inputFileName);
            String html = markdownProcessor.convertToHtml(markdownSource);

            if (outputFileName == null) {
                System.out.println(html);
            } else {
                FileUtils.writeFile(outputFileName, html);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String convertToHtml(String markdownSource) {
        if (markdownSource == null) {
            throw new NullPointerException("Markdown string cannot be null!");
        }
        return markdownHtmlSerializer.serialize(parseMarkdown(markdownSource));
    }

    private RootNode parseMarkdown(String markdownSource) {
        return markdownParser.parse(markdownSource);
    }
}