package com.melnykov;

import com.melnykov.nodes.RootNode;

public interface MarkdownSerializer {
    String serialize(RootNode markdownNode);
}