package com.melnykov;

import java.io.*;

/**
 * This class is a utility class for file operations.
 */
public class FileUtils {

    private FileUtils() {
        // Prevent instantiation
    }

    public static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        StringBuilder sb = new StringBuilder();

        try {
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
        } finally {
            br.close();
        }

        return sb.toString();
    }

    public static void writeFile(String fileName, String data) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
            writer.write(data);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}