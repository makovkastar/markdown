package com.melnykov;

import com.melnykov.nodes.*;

public interface Visitor {
    void visit(RootNode node);

    void visit(HeaderNode node);

    void visit(EmphasizeNode node);

    void visit(StrongNode node);

    void visit(TextNode node);

    void visit(ParagraphNode node);

    void visit(LinkNode node);
}
