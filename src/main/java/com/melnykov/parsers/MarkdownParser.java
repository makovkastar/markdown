package com.melnykov.parsers;

import com.melnykov.MarkdownTag;
import com.melnykov.nodes.*;

import java.util.*;
import java.util.regex.Matcher;

/**
 * This class parses raw markdown string into the markdown node objects structure.
 */
public class MarkdownParser {

    public RootNode parse(String markdownSource) {
        List<Node> children = new ArrayList<Node>();

        Scanner scanner = new Scanner(markdownSource);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            children.add(parseLine(line));
        }

        return new RootNode(children);
    }

    private Node parseLine(String line) {
        // Header
        Matcher matcher = MarkdownTag.HEADER.getMatcher(line);
        if (matcher.matches()) {
            int headerLevel = matcher.group(1).length();
            String text = matcher.group(2);
            return new HeaderNode(text, headerLevel);
        } else { // Paragraph
            return new ParagraphNode(parseChildren(line));
        }
    }

    private List<Node> parseChildren(String line) {
        List<Node> children = new ArrayList<Node>();
        Map<Integer, MarkdownToken> tokens = tokenize(line);

        int position = 0;
        StringBuilder textBuf = new StringBuilder();
        // Iterate through the line char by char and fill the list of child markdown nodes.
        while (position < line.length()) {
            // Check if we have a markdown node starting from this position.
            if (tokens.containsKey(position)) {
                if (textBuf.length() > 0) {
                    // We might have a plain text in the buffer before we met a markdown node.
                    children.add(new TextNode(textBuf.toString()));
                    textBuf.setLength(0);
                }

                MarkdownToken token = tokens.get(position);
                // Place that node to the children list.
                children.add(token.node);
                // Increment position by the length of the node token.
                position += token.length;
            } else {
                // Place a new char to the text buffer.
                textBuf.append(line.charAt(position));
                position++;
            }
        }
        // If we have non-empty text buffer, place the text node to the children list.
        if (textBuf.length() > 0) {
            children.add(new TextNode(textBuf.toString()));
        }

        return children;
    }

    /**
     * @return a map of the {@link MarkdownToken} instances with a token position in the source string as a key.
     */
    private Map<Integer, MarkdownToken> tokenize(String line) {
        Map<Integer, MarkdownToken> tokens = new HashMap<Integer, MarkdownToken>();

        for (MarkdownTag tag : MarkdownTag.values()) {
            Matcher matcher = tag.getMatcher(line);
            // Find all known markdown tags and save them into the map.
            while (matcher.find()) {
                MarkdownToken token;
                int groupLength = matcher.group().length();
                String text = matcher.group(1);
                switch (tag) {
                    case EMPHASIZE:
                        token = new MarkdownToken(groupLength, new EmphasizeNode(text));
                        break;
                    case STRONG:
                        token = new MarkdownToken(groupLength, new StrongNode(text));
                        break;
                    case LINK:
                        String url = matcher.group(2);
                        token = new MarkdownToken(groupLength, new LinkNode(text, url));
                        break;
                    default:
                        throw new IllegalStateException("Unknown markdown tag: " + tag);
                }

                tokens.put(matcher.start(), token);
            }
        }

        return tokens;
    }

    /**
     * This class is a wrapper for the {@link Node} instances, that also contains node length in the original string.
     */
    private static class MarkdownToken {

        final int length;
        final Node node;

        public MarkdownToken(int length, Node node) {
            this.length = length;
            this.node = node;
        }

        @Override
        public String toString() {
            return "MarkdownToken{" +
                    "length='" + length + '\'' +
                    ", node=" + node +
                    '}';
        }
    }
}