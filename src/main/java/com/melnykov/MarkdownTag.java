package com.melnykov;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum MarkdownTag {
    HEADER("^\\s*(#{1,6})(.*)"),
    EMPHASIZE("\\*(?!\\*)([^\\*]+?)\\*(?!\\*)"),
    STRONG("\\*{2}(.*?)\\*{2}"),
    LINK("\\[(.*)\\]\\((.*)\\)");

    final String regexp;

    MarkdownTag(String regexp) {
        this.regexp = regexp;
    }

    public Matcher getMatcher(String string) {
        Pattern pattern = Pattern.compile(regexp);
        return pattern.matcher(string);
    }
}